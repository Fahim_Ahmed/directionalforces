#include "initiator.h" 
 
#define FRICTION_COE 0.3
#define BGCOL 20

//--------------------------------------------------------------
void initiator::setup(){
    
    ofSetFrameRate(60);
	ofBackground(BGCOL);
	ofSetBackgroundAuto(false);

	drawStart = false;
	lastPos = ofVec2f(ofGetMouseX(), ofGetMouseY());
    
    ctr = 1;
}
 
//--------------------------------------------------------------
void initiator::update(){
	int numFallen = fallens.size();
	for(int i = 0; i < numFallen; i++){
		fallens[i].update();

		if(fallens[i].getVelocity().length() >= FRICTION_COE)
            fallens[i].applyForce(calculateFriction(FRICTION_COE, fallens[i]));
        else {
            fallens[i].stop();
        }
	}
}
 
ofVec2f dir, lastDir;
int speed;
ofColor col(240,130,55);

//--------------------------------------------------------------
void initiator::draw(){
	
	ofVec2f mPos(ofGetMouseX(), ofGetMouseY());
	dir = (mPos - lastPos);
	speed = dir.length();
	speed = ofMap(speed, 0, 170, 0, 30, true);
	dir.normalize();
	dir *= speed;

    path.setFilled(false);    
	path.setStrokeWidth(8);
	//path.setStrokeColor(col);
	path.setStrokeColor(col);

    if (path.getOutline().size() > 0){
      //  path.getOutline()[0].draw();		
		path.draw();
		if(drawStart){
			fallen f(mPos.x, mPos.y, 1, 1);
			f.applyForce(dir);
			fallens.push_back(f);

			ofVec2f tmp = dir.getMiddle(lastDir);
			//tmp = tmp - lastPos;
			tmp.normalize();
			tmp *= speed;

			fallen f2((lastPos.x + mPos.x)*0.5, (lastPos.y + mPos.y)*0.5, 1, 1);
			f2.applyForce(tmp);
			fallens.push_back(f2);
		}
    }
	
	int numFallen = fallens.size();
	for(int i = 0; i < numFallen; i++){
		fallens[i].draw(col);
		fallens[i].edgeBounce();
	}
	
	lastDir = dir;
	lastPos = ofVec2f(ofGetMouseX(), ofGetMouseY());
}

ofVec2f initiator::calculateFriction(float c, fallen target) {
    friction = target.getVelocity();
    friction *= -1;
    friction.normalize();
    friction *= c;

    return friction;
}
 
//--------------------------------------------------------------
void initiator::keyPressed(int key){
 
}
 
//--------------------------------------------------------------
void initiator::keyReleased(int key){
	ofClear(BGCOL);
	path.clear();
	bPath.clear();
}
 
//--------------------------------------------------------------
void initiator::mouseMoved(int x, int y ){
 
}
 
//--------------------------------------------------------------
void initiator::mouseDragged(int x, int y, int button){
 
    
    lineOrig.addVertex( ofPoint (x,y));
    
    ofPoint p = ofPoint(x,y);
    ctr++;
    pts[ctr] = p;
    if (ctr == 4)
    {
		// move the endpoint to the middle of the line joining the second control point of the first Bezier segment 
		// and the first control point of the second Bezier segment

        pts[3] = ofPoint((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); 
        
        if (path.getCommands().size() == 0) path.moveTo(pts[0]);
        path.bezierTo(pts[1],pts[2], pts[3]);
        
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }

	bPath = path;
    
}
 
//--------------------------------------------------------------
void initiator::mousePressed(int x, int y, int button){
	drawStart = true;

    ctr = 0;
    pts[0] = ofPoint(x,y);
    path.clear();
	bPath.clear();
    
    lineOrig.addVertex( ofPoint (x,y));
    
}
 
//--------------------------------------------------------------
void initiator::mouseReleased(int x, int y, int button){
	drawStart = false;
}
 
//--------------------------------------------------------------
void initiator::windowResized(int w, int h){
 
}
 
//--------------------------------------------------------------
void initiator::gotMessage(ofMessage msg){
 
}
 
//--------------------------------------------------------------
void initiator::dragEvent(ofDragInfo dragInfo){ 
 
}