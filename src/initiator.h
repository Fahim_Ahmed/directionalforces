#pragma once

#include "ofMain.h"
#include "fallen.h"

class initiator : public ofBaseApp{
	private:
		vector<fallen>	fallens;
		bool			drawStart;
		ofVec2f			lastPos;
		ofPoint			pts[5];
		unsigned int	ctr;
		ofPath			path, bPath;
		ofPolyline		lineOrig;

		ofVec2f			friction;

	public:
		void setup();
		void update();
		void draw();

		ofVec2f calculateFriction(float c, fallen target);

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
